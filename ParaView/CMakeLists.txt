include(UseLATEX)

set (extras_tex)
set (extras_image_dirs)
if (ENABLE_PARAVIEW_GUIDE_EXTRAS)
  file(GLOB extras_tex RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} Extras/*.tex)
  if(NOT extras_tex)
    message(SEND_ERROR "You've requested 'Extras', but you don't seem to have source files for the extras chapters!!!")
  endif()
  if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/Extras/Images)
    set (extras_image_dirs Extras/Images)
  endif()
endif()

set(name "ParaViewUsersGuide")
add_latex_document(
  ${name}.tex
  INPUTS
    TitlePage.tex
    Insight.sty
    InsightSoftwareGuide.cls
    Abbreviations.tex
    Animation.tex
    AxesGrid.tex
    CAVEDisplay.tex
    ColorMapping.tex
    Contributors.tex
    #CommandLineArguments.tex
    ComparativeViews.tex
    ContributorNotes.tex
    CustomDefaultSettings.tex
    DataIngestion.tex
    DisplayingData.tex
    Draft.tex
    FilteringData.tex
    FilteringRecommendations.tex
    Final.tex
    Introduction.tex
    LatexMacros.tex
    Links.tex
    MemoryInspector.tex
    MultiblockInspector.tex
    ParallelDataVisualization.tex
    ParallelRendering.tex
    ParaViewUsersGuide.tex
    PythonProgrammableFilter.tex
    PropertiesPanel.tex
    SavingResults.tex
    SelectingData.tex
    Settings.tex
    TiledDisplay.tex
    UnderstandingData.tex
    VTKDataModel.tex
    VTKNumPyIntegration.tex
    menukeys.sty
    LanguageDefinitions.tex
    ConfiguredOptions.tex
    ${extras_tex}
  CONFIGURE
    ConfiguredOptions.tex
  INDEX_NAMES keyword python ui
  BIBFILES References.bib
  IMAGE_DIRS Images ${extras_image_dirs}
  DEPENDS multicols.sty multind.sty
  USE_INDEX
)

add_test(
  NAME TestParaViewGuide
  COMMAND ${CMAKE_COMMAND} 
    -D LOG_FILE_NAME:STRING=${CMAKE_CURRENT_BINARY_DIR}/ParaViewUsersGuide.log
    -P ${CMAKE_BINARY_DIR}/CMake/TestLatexLog.cmake)
set_tests_properties(TestParaViewGuide PROPERTIES
  FAIL_REGULAR_EXPRESSION "Error;Warning")

add_test(
  NAME TestParaViewImages
  COMMAND ${CMAKE_COMMAND}
    -D IMAGE_DIRS:STRING=${CMAKE_CURRENT_SOURCE_DIR}/Images\;${CMAKE_CURRENT_SOURCE_DIR}/Extras/Images
    -P ${CMAKE_SOURCE_DIR}/CMake/TestImagePPI.cmake)
set_tests_properties(TestParaViewImages PROPERTIES
  FAIL_REGULAR_EXPRESSION "failed the PPI test")

if (__BUILDBOT_UPLOAD_FILES)
  include(CopyToUpload)
  copy_to_upload(${name}.pdf "ParaViewGuide-CE-${MOST_RECENT_TAG}.pdf")
endif()

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${name}.pdf DESTINATION doc)
