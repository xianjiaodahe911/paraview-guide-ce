The \ui{Properties} panel is perhaps the most often used panel in
\paraview. This is the panel you would use to change properties
on modules in the visualization pipeline, including sources and filters, to control how
they are displayed in views using the \ui{Display} properties, and
to customize the view itself. In this chapter, we take a closer look at the
\ui{Properties} panel to understand how it works.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/PropertiesPanelComponents.png}
\caption{\ui{Properties} Panel in \paraview}
\label{fig:PropertiesPanel}
\end{center}
\end{figure}


\section{Anatomy of the \texttt{Properties} panel}

Before we start dissecting the \ui{Properties}, remember that the \ui{Properties}
panel works on \emph{active} objects, i.e., it shows the properties for the active
source and active view, as well as the display properties, if any, for active source in the active view.

\subsection{Buttons}
Figure~\ref{fig:PropertiesPanel} shows the various parts of the
\ui{Properties} panel. At the top is the group of buttons that let you accept,
reject your changes to the panel or \ui{Delete} the active source.

\begin{didyouknow}
  You can delete multiple sources by selecting them using the
  \keys{\ctrl} (or \keys{\cmdmac}) key when selecting in the \ui{Pipeline
  Browser} and then clicking on the \ui{Delete} button.
  Sometimes, the \ui{Delete} button may be disabled. That happens when the
  selected source(s) have other filters connected to them. You will need to
  delete those filters first.
\end{didyouknow}

\subsection{Search box}
\label{sec:PropertiesPanel:SearchBox}

The \ui{Search} box allows you to search for a property by using the name or the
label for the property. Simply start typing text in the \ui{Search} box, and the
panel will update to show widgets for the properties matching the text.

The \ui{Properties} panel has two modes that control the verbosity of the panel:
default and advanced. In the default mode, a smaller subset of the available
properties is shown. These are generally the frequently used properties for the
pipeline modules. In advanced mode, you can see all the available properties.
You can toggle between default and advanced modes using the
\icon{Images/pqAdvanced26.png} button next
the \ui{Search} box.

When you start searching for a property by typing text in the \ui{Search} box,
irrespective of the current mode of the panel (i.e., default or advanced), all
properties that match the search text will be shown.

\begin{didyouknow}
The \ui{Search} box is a recurring widget in \paraview. Several other panels
and dialog boxes, including the \ui{Settings} dialog and the \ui{Color Map Editor},
show a similar widget. Its behavior is also exactly the same as in the case of the
\ui{Properties} panel. Don't forget that the \icon{Images/pqAdvanced26.png}
button can be used to toggle between default and advanced modes.
\end{didyouknow}

\subsection{Properties}

The \ui{Properties}, \ui{Display}, and \ui{View} sections in the panel
show widgets for properties on the active source, its display
properties in the active view, and the properties for the active view,
respectively. You can collapse/expand each of these sections by
clicking on the section name.

To the right of each section name is a set of four buttons. Clicking the
\icon{Images/CopyButton.png} copies the current set of property values to the
clipboard while clicking the \icon{Images/PasteButton.png} will paste those
property values into another compatible panel section. Note that the paste icon
is enabled only for panel sections where the copied properties can be pasted.

The next two buttons \icon{Images/ReloadButton.png} and
\icon{Images/SaveAsDefaultButton.png} enable customizing the default values
used for those properties. Refer to Chapter~\ref{chapter:CustomDefaultSettings}
to learn more about customizing default property values.

\section{Customizing the layout}
\label{sec:PropertiesPanelLayoutSettings}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/PropertiesPanelSettings.png}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UsersGuide_PropertiesPanelTwoWindows.png}
\caption{Options for customizing the \ui{Properties} panel layout using the
\ui{Settings} (left). View properties in a separate dock panel (right).}
\label{fig:PropertiesPanelSettings}
\end{center}
\end{figure}

The \ui{Properties} panel, by default, is set up to show the source, display, and
view properties on the same panel. You may, however, prefer to have each of these
sections in a separate dockable panel. You can indeed doso using the
\ui{Settings} dialog accessible from the \menu{Edit > Settings} menu.

On the \ui{General} tab search of the \texttt{properties panel} using the
\ui{Search} box, you should see the setting that lets you pick whether to
combine all the sections in one (default), to separate out either the
\ui{Display} or {View} sections in a  panel, or to create separate panels
for each of the sections. You will need to restart \paraview for
this change to take effect. Also, since the \ui{Apply} and \ui{Reset} buttons only
apply to the \ui{Properties} section, they will only be shown in the dock panel
that houses it.
