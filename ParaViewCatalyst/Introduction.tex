Computer simulations are growing in sophistication and producing results of ever greater
fidelity. This trend has been enabled by advances in numerical methods and increasing
computing power. Yet these advances come with several costs including massive increases in
data size, difficulties examining output data, challenges in configuring simulation runs, and
difficulty debugging running codes. For computer simulation to provide the many significant
benefits that have been exhaustively documented (“The Opportunities and Challenges of
Exascale Computing: Summary Report of the Advanced Scientific Computing Advisory
Committee (ASCAC) Subcommittee,”
\url{http://science.energy.gov/\~/media/ascr/ascac/pdf/reports/Exascale\_subcommittee\_report.pdf},
Fall 2010), it's imperative to address these issues.
The ParaView Catalyst library is a system that addresses such challenges. It is designed to be easily
integrated directly into large-scale numerical codes. Built on and designed to interoperate with
the standard visualization toolkit VTK and scalable ParaView application, it enables
simulations to intelligently perform analysis, generate relevant output data, and visualize results
concurrent with a running simulation. This ability to concurrently visualize and analyze data from simulations
is referred to synonymously as \textit{in situ} processing, co-processing, co-analysis, concurrent visualization,
and co-visualization. Thus ParaView Catalyst is often referred to as a co-processing, or
\textit{in situ}, library for high-performance computing (HPC).
In the remainder of this Introduction section, we will motivate the use of Catalyst, and describe
an example workflow to demonstrate just how easy Catalyst is to use in practice.

\section{Motivation}
Computing systems have been increasing in speed and capacity for many years now. Yet not all
of the various subsystems which make up a computing environment have been advancing
equally as fast. This has led to many changes in the way large-scale computing is performed.
For example, simulations have long been scaling towards millions of parallel
computing cores in recognition that serial processing is inherently limited by the bottleneck of a
single processor. As a result, parallel computing methods and systems are now central to
modern computer simulation. Similarly, with the number of computing cores
increasing to address bigger problems, IO is now becoming a limiting factor as Table~\ref{table:exascaleroadmap}
below indicates. While the increase in FLOPS between FY2010 and 2018 is expected to be on the order of 500,
IO bandwidth is increasing on the order of 100 times.

\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
 & 2010 & 2018 & Factor Change \\
\hline\hline
Peak FLOP Rate & 2 PF/s & 1 EF/s & 500 \\
Input/Output Bandwidth & 0.2 TB/s & 20 TB/s & 100 \\ [1ex] %[1ex] adds vertical space
\hline
\end{tabular}
\begin{center}
\caption{Potential exascale computer performance. Source: DOE Exascale Initiative
Roadmap, Architecture and Technology Workshop, San Diego, December, 2009.}
\label{table:exascaleroadmap}
\end{center}
\end{table}

This divergence between computational power and IO bandwidth has profound implications on
the way future simulation is performed. In the past it was typical to break the simulation process
into three pieces: pre-processing (preparing input); simulation (execution); and post-processing
(analyzing and visualizing results). This workflow is fairly simple and treats these three tasks
independently, simplifying the development of new computational tools by relying on a loose
coupling via data file exchange between each of the pieces. For example, pre-processing systems
are typically used to discretize the domain, specify material properties, boundary
conditions, solver parameters, etc.
finally writing this information out into one or more input files to the simulation code.
Similarly, the simulation process typically writes output files which are read in by the
post-processing system. However limitations in IO bandwidth throw a monkey wrench into this
process, as the time to read and write data on systems with relatively large computational power
is becoming a severe bottleneck to the simulation workflow. Savings can be obtained even for
desktop systems with a small amount of parallel processes. This is shown in the figure below for
a 6 process run on a desktop machine performing different analysis operations as well as IO. It
is clear that the abundance of computational resources (cores) results in relatively rapid
analysis, which even when taken together are faster than the time it takes for the simulation
code to save a full dataset.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in]{Images/filtercomputetimes.png}
\caption{Comparison of compute time in seconds for certain analysis operations vs. saving
the full dataset for a 6 process run on a desktop machine.}
\label{fig:filtercomputetimes}
\end{center}
\end{figure}

The root problem is that due to the ever increasing computational power available
on the top
HPC machines, analysts are now able to run simulations with increasing fidelity. Unfortunately
this increase in fidelity corresponds to an increase in the data generated by the simulation. Due
to the divergence between IO and computational capabilities, the resulting data bottleneck is
now negatively impacting the simulation workflow. For large problems, gone are the days when
data could be routinely written to disk and/or copied to a local workstation or visualization
cluster. The cost of IO is becoming prohibitive, and large-scale simulation in the era of cheap
FLOPS and expensive IO requires new approaches.

One popular, but crude approach relies on configuring the simulation process to save results
less frequently (for example, in a time-varying analysis, every tenth time step may be saved,
meaning that 90\% of the results are simply discarded). However even this strategy is
problematic: It is possible that a full save of the simulation data for even a single time step may
exceed the capacity of the IO system, or require too much time to be practical.

A better approach, and the approach that ParaView Catalyst takes, is to change the traditional three-step
simulation workflow of pre-processing, simulation, and post-processing (shown in Figure~\ref{fig:threestepworkflow})
to one that integrates post-processing directly into the simulation process as shown in
Figure~\ref{fig:catalystworkflow}. This integration of simulation with post-processing provides several key advantages. First, it
avoids the need to save out intermediate results for the purpose of post-processing; instead
the post-processing work can be performed \textit{in situ}
as the simulation is running, This saves considerable
time as illustrated in Figure~\ref{fig:scalingplots}.

\begin{figure}[htb]
\centering
\subfloat[]{\includegraphics[width=4.8in]{Images/threestepworkflow.png}\label{fig:threestepworkflow}}\\
\subfloat[]{\includegraphics[width=4in]{Images/catalystworkflow.png}\label{fig:catalystworkflow}}
\caption{Traditional \protect\subref{fig:threestepworkflow} and ParaView Catalyst \protect\subref{fig:catalystworkflow} workflows.}
\label{fig:workflows}
\end{figure}

\begin{figure}[htb]
\centering
\subfloat[]{\includegraphics[width=3in]{Images/cthdumpscaling.png}}
\subfloat[]{\includegraphics[width=3in]{Images/cthcatalystscaling.png}}
\caption{Comparison of full workflow for CTH with post-processing results: (a) full
workflow vs. with \textit{in situ} processing with ParaView Catalyst (b). Results courtesy of Sandia National Laboratories.}
\label{fig:scalingplots}
\end{figure}

Further, instead of saving full datasets to disk, IO can be reduced by extracting only relevant
information. Data extracts like iso-contours, data slices, or streamlines are
generally orders of magnitude smaller than the full dataset (Figure~\ref{fig:outputsizes}).
Thus writing out extracts significantly reduces the total IO cost.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in]{Images/outputsizes.png}
\caption{Comparison of file size in bytes for saving full dataset vs. saving specific analysis outputs.}
\label{fig:outputsizes}
\end{center}
\end{figure}

Finally, unlike blind subsampling of results, using an integrated approach it becomes possible to
analyze the current state of the simulation and save only information pertinent to the scientific
query at hand. For example, it is possible to identify the signs of a forming shock and then save
only that information in the neighborhood of the shock.

There are other important applications that address the complexity of the simulation process.
Using co-processing it is possible to monitor the progress of
the simulation, and ensure that it is
progressing in a valid way. It is not uncommon for a long running simulation (maybe days or
longer in duration) to be tossed out because initial conditions, boundary conditions or solution parameters
were specified incorrectly. By
checking intermediate results it's possible to catch mistakes like
these and terminate such runs before they incur excessive costs. Similarly, co-processing
enables improved debugging of simulation codes. Visualization can be used to great effect to identify
regions of instability or numerical breakdown.

ParaView Catalyst was created as a library to achieve the integration of simulation and post-processing. It
has been designed to be easy to use and introduces minimal disruption into numerical codes. It
leverages standard systems such as VTK and ParaView (for post-processing) and utilizes
modern scientific tools like Python for control and analysis. Overall Catalyst has been shown to
dramatically increase the effectiveness of the simulation workflow by reducing the amount of IO,
thereby reducing the time to gain insight into a given problem, and more efficiently utilizing
modern HPC environments with abundant FLOPS and restricted IO bandwidth.

\section{Example Workflow}
Figure~\ref{fig:examplecatalystworkflow} demonstrates a typical workflow using Catalyst for
\textit{in situ} processing. In this
figure it is assumed that Catalyst has already been integrated into the simulation code (see
Section~\ref{chapter:DeveloperSection}
for details on how to integrate Catalyst). The workflow is initiated by creating a
Python script using ParaView's GUI which specifies the desired output from the simulation.
Next, when the simulation starts it loads this script; then during execution any analysis and
visualization output is generated in synchronous fashion (i.e., while the simulation is running).
Catalyst can produce images/screenshots, compute statistical quantities, generate plots, and
extract derived information such as polygonal data or iso-surfaces to visualize geometry and/or
data.
\begin{figure}[htb]
\begin{center}
\includegraphics[width=5in]{Images/fullcatalystworkflow.png}
\caption{\textit{In situ} workflow with various Catalyst outputs.}
\label{fig:examplecatalystworkflow}
\end{center}
\end{figure}

Catalyst has been used by a variety of simulation codes.
An arbitrary list of these codes includes PHASTA from UC Boulder, Hydra-TH, MPAS-O,
XRAGE, NPIC and VPIC from LANL, CREATE-AV\textsuperscript{TM} Helios from the Army's Aeroflightdynamics Directorate, and CTH,
Albany and the Sierra simulation framework from Sandia, H3D from UCSD, and Code Saturne from EDF
have all been instrumented to use Catalyst. Some example outputs are shown in
Figure~\ref{fig:catalystexampleoutput}.

\begin{figure}[htb]
\begin{center}
\subfloat[PHASTA]{\includegraphics[width=3in]{Images/phasta.png}}\,
\subfloat[CREATE-AV\textsuperscript{TM}Helios]{\includegraphics[width=3in]{Images/helios.png}\label{fig:helios}} \\
\subfloat[Code Saturne]{\includegraphics[width=3in]{Images/codesaturne.png}}\,
\subfloat[CTH]{\includegraphics[width=3in]{Images/cth.png}\label{fig:cth}}
\caption{Various results from simulation codes linked with ParaView Catalyst. Note that post-processing with different packages was performed with \protect\subref{fig:helios} and \protect\subref{fig:cth}.}
\label{fig:catalystexampleoutput}
\end{center}
\end{figure}


Of course Catalyst is not necessarily applicable in all situations. First, if significant reductions in
IO are important, then it's important that the specified analysis and visualization pipelines
invoked by Catalyst actually produce reduced data size. Another important consideration is
whether these pipelines scale appropriately. If they do not, then a large-scale simulation may
bog down during co-processing, detrimentally impacting total analysis cycle time. However, both
the underlying ParaView and VTK systems have been developed with parallel scaling in mind,
and generally perform well in most applications. Figure~\ref{fig:filtersscaling} shows two scale
plots for two popular algorithms: slicing through a dataset, and decimating large meshes (e.g., reducing the
size of an output iso-contour).

\begin{figure}[htb]
\begin{center}
\subfloat[Slice]{\includegraphics[width=3in]{Images/slicescaling.png}}
\subfloat[Decimate]{\includegraphics[width=3in]{Images/decimatescaling.png}} \\
\caption{Scaling performance of VTK filters.}
\label{fig:filtersscaling}
\end{center}
\end{figure}


Such stellar performance is typical of
VTK algorithms, but we recommend that you confirm this
behavior for your particular analysis pipeline(s).

\section{Further Information}
The following are various links of interest related to Catalyst:
\begin{itemize}
\item \url{www.paraview.org} The main ParaView page with links to wikis, code, documentation,
etc.
\item \url{www.paraview.org/download} The main ParaView download
page. Useful for installing ParaView on local machines for creating Catalyst scripts and
viewing Catalyst output. The source code is also available which contains Catalyst
specific example code useful for developers.
\item \url{www.paraview.org/in-situ} The main page for ParaView Catalyst.
\item \url{paraview@paraview.org} The mailing list for general ParaView and Catalyst support.
\item \url{www.cinemascience.org} The main page for information on Cinema, an image-based method for doing
\textit{in situ} analysis and visualization.
\end{itemize}

The remainder of this guide is broken up into three main sections. Section~\ref{chapter:UserSection} addresses users
that wish to use simulation codes that have already been instrumented with Catalyst.
Section~\ref{chapter:DeveloperSection}
is for developers who wish to instrument their simulation code. Section~\ref{chapter:BuildSection}
focuses on those users
who wish to install and maintain Catalyst on their computing systems.
